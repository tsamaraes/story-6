from django.test import TestCase
from django.test.client import Client
from django.urls import resolve
from .models import Status
from .forms import StatusForm
from .views import status
from django.apps import apps
from homepage.apps import HomepageConfig

class story6Tests(TestCase):
    def test_story6_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_story6_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'status.html')

    def test_story6_func(self):
        found = resolve('/')
        self.assertEqual(found.func.__name__, status.__name__)

    def test_create_new_status(self):
        new = Status.objects.create(status = 'yuk bisa yuk')
        self.assertTrue(isinstance(new, Status))
        self.assertTrue(new.__str__(), new.status)
        count_status = Status.objects.all().count()
        self.assertEqual(count_status, 1)

    def test_display_status(self):
        status = "semangat"
        data = {'stats' : status}
        post_data = Client().post('/', data)
        self.assertEqual(post_data.status_code, 200)

    def test_application(self):
        self.assertEqual(HomepageConfig.name, 'homepage')
        self.assertEqual(apps.get_app_config('homepage').name, 'homepage')

    def test_hello(self):
        response = Client().get('/')
        response_content = response.content.decode('utf-8')
        self.assertIn("Hello", response_content)

    def test_form_validation(self):
        response = self.client.post('', data={'status' : 'Status'})
        response_content = response.content.decode()
        self.assertIn(response_content, 'Status')

