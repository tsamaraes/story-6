from django.urls import path
from . import views
from .views import status

app_name = 'homepage'

urlpatterns = [
    path('', views.status, name='status'),
]