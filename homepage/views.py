from django.shortcuts import render
from django.shortcuts import redirect
from .models import Status
from . import forms
from .forms import StatusForm

def status(request):
	stats = Status.objects.all()
	if (request.method == 'POST'):
		form = forms.StatusForm(request.POST)
		if (form.is_valid()):
			form.save()
			return redirect('/')
	else:
		form = forms.StatusForm()
	return render(request, 'status.html', {'stats': stats, 'form':form})
