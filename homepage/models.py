from django.db import models
from datetime import datetime  

class Status(models.Model):
    status = models.TextField('status', max_length = 100)
    time = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return self.status