from django.test import TestCase
from django.test.client import Client
from django.urls import resolve

# Create your tests here.
class challengeTests(TestCase):
    def test_challenge_url_is_exist(self):
        response = Client().get('/challenge/')
        self.assertEqual(response.status_code,200)

    def test_challenge_template(self):
        response = Client().get('/challenge/')
        self.assertTemplateUsed(response, 'index.html')

    def test_npm(self):
        response = Client().get('/challenge/')
        response_content = response.content.decode('utf-8')
        self.assertIn("1806173563", response_content)